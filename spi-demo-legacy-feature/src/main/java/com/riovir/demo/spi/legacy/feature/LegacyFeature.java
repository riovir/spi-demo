package com.riovir.demo.spi.legacy.feature;

import com.riovir.demo.spi.api.Feature;

public class LegacyFeature implements Feature {
	@Override
	public String greetUsers() {
		return "Um... hi!";
	}
}
