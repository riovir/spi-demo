package com.riovir.demo.spi.foo.plugin;

import java.util.function.Consumer;
import java.util.stream.Stream;

import com.riovir.demo.spi.api.Plugin;

public class FooPlugin implements Plugin {
	@Override
	public void putBusinessValueIn(Consumer<String> bucket) {
		Stream.of(
				"- car rental",
				"- bed and breakfast",
				"- travelling"
		).forEach(bucket);
	}
}
