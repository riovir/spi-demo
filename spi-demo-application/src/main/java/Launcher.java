import java.util.Iterator;
import java.util.ServiceLoader;

import com.riovir.demo.spi.api.Feature;
import com.riovir.demo.spi.api.Plugin;

public class Launcher {
	public static void main(String[] args) {
		System.err.println("This application only depends on spi-demo-api and spi-demo-application");
		System.err.println("But also needs either legacy-feature or new-feature to work");
		
		Iterator<Feature> iterator = ServiceLoader.load(Feature.class).iterator();
		Feature feature = iterator.hasNext() ? iterator.next() : null;
		if (feature == null) {
			System.err.println("Feature not found: I could keep running but I won't.");
			return;
		}
		if (iterator.hasNext()) {
			System.err.println("Feature onflict: I'm confused how to greet people");
			return;
		}
		System.out.println(feature.greetUsers());
		
		System.out.println("Plugins delivered the following extras:");
		ServiceLoader.load(Plugin.class)
				.forEach(plugin -> plugin.putBusinessValueIn(System.out::println));
	}
}
