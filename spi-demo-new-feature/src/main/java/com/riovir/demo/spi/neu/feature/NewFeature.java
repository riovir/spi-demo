package com.riovir.demo.spi.neu.feature;

import com.riovir.demo.spi.api.Feature;

public class NewFeature implements Feature {
	@Override
	public String greetUsers() {
		return "Welcome to this SPI demo";
	}
}
